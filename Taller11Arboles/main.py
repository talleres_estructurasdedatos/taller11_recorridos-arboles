import json

class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.left = None
        self.right = None

    def preorden(self, resultado_list):
        resultado_list.append(self.valor)  # O(1) - Agregar un elemento a la lista
        if self.left is not None:
            self.left.preorden(resultado_list)  # O(n) en el peor caso
        if self.right is not None:
            self.right.preorden(resultado_list)  # O(n) en el peor caso

    def inorden(self, resultado_list):
        if self.left is not None:
            self.left.inorden(resultado_list)  # O(n) en el peor caso
        resultado_list.append(self.valor)  # O(1) - Agregar un elemento a la lista
        if self.right is not None:
            self.right.inorden(resultado_list)  # O(n) en el peor caso

    def postorden(self, resultado_list):
        if self.left is not None:
            self.left.postorden(resultado_list)  # O(n) en el peor caso
        if self.right is not None:
            self.right.postorden(resultado_list)  # O(n) en el peor caso
        resultado_list.append(self.valor)  # O(1) - Agregar un elemento a la lista

# Creación del árbol
arbol = Nodo(1)  # O(1)
arbol.left = Nodo(2)  # O(1)
arbol.right = Nodo(3)  # O(1)
arbol.left.left = Nodo(4)  # O(1)
arbol.left.right = Nodo(5)  # O(1)

# Recorridos del árbol
resultado_preorden = []  # O(1)
arbol.preorden(resultado_preorden)  # O(n) en el peor caso

resultado_inorden = []  # O(1)
arbol.inorden(resultado_inorden)  # O(n) en el peor caso

resultado_postorden = []  # O(1)
arbol.postorden(resultado_postorden)  # O(n) en el peor caso

# Impresión de los recorridos
print("El recorrido del árbol es ", json.dumps(resultado_preorden))  # O(1)
print("El recorrido del árbol es ", json.dumps(resultado_inorden))  # O(1)
print("El recorrido del árbol es ", json.dumps(resultado_postorden))  # O(1)
